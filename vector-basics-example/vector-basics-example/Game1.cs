﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace vector_basics_example
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // Textures for our animals
        Texture2D hunter;
        Texture2D prey;

        // Vector variables to hold animal positions
        Vector2 posHunter;
        Vector2 posPrey;

        // Font used to draw animal labels
        SpriteFont labelFont;

        // Offset used in vector addition to calculate label position
        Vector2 labelOffset;

        // Angle our Hunter is drawn at
        float angle;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            // Load animal sprites
            hunter = Content.Load<Texture2D>("bear");
            prey = Content.Load<Texture2D>("chicken");

            // Set our animals' positions
            posHunter = new Vector2(100, 100);
            posPrey = new Vector2(500, 200);

            // Load font for labels
            labelFont = Content.Load<SpriteFont>("mainFont");

            // Set up offset for label positioning
            labelOffset = new Vector2(0, -50);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            // Distance vector to hold how far away and in what direction our prey is
            // Hunter is our point of reference, so that is what is subtracted.
            Vector2 distance = posPrey - posHunter;

            // Create direction vector, initially the same as distance
            Vector2 direction = distance;

            // Normalise changes this vector into length of 1
            // This will allow us to use it for constant speed calculations
            direction.Normalize();

            // We can give our Hunter a set speed
            float speed = 100;

            // We can scale this by the time in the last frame to 
            // determine how far our hunter should move this frame
            float timeThisFrame = (float)gameTime.ElapsedGameTime.TotalSeconds;
            float amountToMove = speed * timeThisFrame;

            // Move our hunter closer to the prey using vector addition
            // We multiply direction (length 1) by the amount we should actually move
            // to move that far in that specific direction
            posHunter = posHunter + direction * amountToMove;

            // Use direction to calculate the angle our hunter should turn
            angle = (float)Math.Atan2(direction.Y, direction.X);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();

            // Draw animal sprites at their vector positions
            spriteBatch.Draw(hunter, position: posHunter, rotation: angle);
            spriteBatch.Draw(prey, position: posPrey);

            // Calculate the label positions using vector addition
            // Add the animal's position plus an offset to get the label's position
            Vector2 posHunterLabel = posHunter + labelOffset;
            Vector2 posPreyLabel = posPrey + labelOffset;

            // CHALLENGE: Try adjusting the position of the label to center it horizontally on the animal

            // Draw labels on animals
            spriteBatch.DrawString(labelFont, "hunter", posHunterLabel, Color.White);
            spriteBatch.DrawString(labelFont, "prey", posPreyLabel, Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
